package com.pyc.service;

import com.pyc.entity.Fruit;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pyc.vo.BarVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pyc
 * @since 2021-04-11
 */
public interface FruitService extends IService<Fruit> {

    public BarVo GetBarVos();
}
