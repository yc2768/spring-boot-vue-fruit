package com.pyc.service.impl;

import com.pyc.entity.Fruit;
import com.pyc.mapper.FruitMapper;
import com.pyc.service.FruitService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pyc.util.DataUtil;
import com.pyc.vo.BarSeriesVo;
import com.pyc.vo.BarVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pyc
 * @since 2021-04-11
 */
@Service
public class FruitServiceImpl extends ServiceImpl<FruitMapper, Fruit> implements FruitService {

    @Autowired
    private FruitMapper fruitMapper;

    @Override
    public BarVo GetBarVos() {
        BarVo barVo=new BarVo();
        List<String> names=new ArrayList<>();
        List<BarSeriesVo> barSeries=new ArrayList<>();
        List<Fruit> fruits=this.fruitMapper.selectList(null);
        for (Fruit fruit:fruits)
        {
            System.out.println("fruitName:"+fruit.getName());
            names.add(fruit.getName());
            BarSeriesVo barSeriesVo=new BarSeriesVo();
            barSeriesVo.setValue(fruit.getSale());
            barSeriesVo.setItemStyle(DataUtil.createItemStyle(fruit.getSale()));
            barSeries.add(barSeriesVo);
        }
        barVo.setNames(names);
        barVo.setBarSeries(barSeries);
        return barVo;
    }
}
