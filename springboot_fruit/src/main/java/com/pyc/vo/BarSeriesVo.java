package com.pyc.vo;

import lombok.Data;

import java.util.Map;

@Data
public class BarSeriesVo {
    private  Integer value;
    private Map<String,String> itemStyle;
}
