package com.pyc.controller;


import com.pyc.entity.Fruit;
import com.pyc.service.FruitService;
import com.pyc.vo.BarVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author pyc
 * @since 2021-04-11
 */
@RestController
@RequestMapping("//fruit")
public class FruitController {

    @Autowired
    private FruitService fruitService;

    @GetMapping("/list")
    public List<Fruit> list(){
        return  this.fruitService.list();
    }

    @PostMapping("/add")
    public boolean add(@RequestBody Fruit fruit){
        return this.fruitService.save(fruit);
    }

    @DeleteMapping("/delete/{id}")
    public boolean delete(@PathVariable("id") Integer id)
    {
        return  this.fruitService.removeById(id);
    }

    @GetMapping("/find/{id}")
    public  Fruit find(@PathVariable("id") Integer id)
    {
        return  this.fruitService.getById(id);
    }

    @PutMapping("/update")
    public boolean update(@RequestBody Fruit fruit)
    {
      return  this.fruitService.updateById(fruit);

    }

    @GetMapping("/fruitbar")
    public BarVo barVO(){
        return this.fruitService.GetBarVos();
    }

}

