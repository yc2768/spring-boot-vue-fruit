package com.pyc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * @author Pyc
 */
@SpringBootApplication
@MapperScan("com.pyc.mapper")
public class FruitApplication {

    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(FruitApplication.class, args);
        Environment environment = context.getBean(Environment.class);
        System.out.println("访问链接：http://localhost:" +environment.getProperty("server.port"));
    }

}
