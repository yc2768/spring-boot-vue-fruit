package com.pyc.mapper;

import com.pyc.entity.Fruit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pyc
 * @since 2021-04-11
 */
public interface FruitMapper extends BaseMapper<Fruit> {

}
