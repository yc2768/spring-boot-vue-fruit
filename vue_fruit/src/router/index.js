import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import FruitList from '../views/FruitList'
import AddFruit from '../views/AddFruit'
import EditFruit from '../views/EditFruit'
import FruitBar from '../views/FruitBar'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/fruitlist',
    name: 'FruitList',
    component: FruitList
  },
  {
    path: '/addfruit',
    name: 'AddFruit',
    component: AddFruit
  },
  {
    path: '/editfruit',
    name: 'EditFruit',
    component: EditFruit
  },
  {
    path: '/fruitbar',
    name: 'FruitBar',
    component: FruitBar
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
