/*
 Navicat Premium Data Transfer

 Source Server         : mysql5
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : fruitdatabase

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 11/04/2021 21:08:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fruit
-- ----------------------------
DROP TABLE IF EXISTS `fruit`;
CREATE TABLE `fruit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `sale` int(11) DEFAULT NULL,
  `icon` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fruit
-- ----------------------------
INSERT INTO `fruit` VALUES (1, '苹果', 50, 'https://img01.jituwang.com/190911/256514-1Z91111060126.jpg');
INSERT INTO `fruit` VALUES (2, '香蕉', 133, 'https://img01.jituwang.com/191005/256490-19100520245827.jpg');
INSERT INTO `fruit` VALUES (3, '橘子', 299, 'https://img01.jituwang.com/180121/256726-1P12114302738.jpg');
INSERT INTO `fruit` VALUES (6, '火龙果', 133, 'https://pic01.jituwang.com/190911/256514-1Z91111015753-lp.jpg');
INSERT INTO `fruit` VALUES (15, '芒果', 400, 'https://pic01.jituwang.com/190911/256514-1Z911110S714-lp.jpg');
INSERT INTO `fruit` VALUES (16, '桃子', 620, 'https://img01.jituwang.com/180824/256601-1PR41PH625.jpg');
INSERT INTO `fruit` VALUES (18, '树莓', 200, 'https://img01.jituwang.com/201113/165271-20111312092188.jpg');
INSERT INTO `fruit` VALUES (19, '蓝莓', 800, 'https://img01.jituwang.com/201113/165271-20111312024194.jpg');
INSERT INTO `fruit` VALUES (20, '草莓', 600, 'https://img01.jituwang.com/201113/165271-20111311292166.jpg');
INSERT INTO `fruit` VALUES (23, '西瓜', 800, 'https://img01.jituwang.com/201113/165271-20111311024188.jpg');

SET FOREIGN_KEY_CHECKS = 1;
